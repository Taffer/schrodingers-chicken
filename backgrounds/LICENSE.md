# Background Licenses

 `colored_castle.png` by [Kenney](https://kenney.nl/) in the
 [Background Elements Redux](https://kenney.nl/assets/background-elements-redux)
 pack.

 > [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
 > license.
