extends Node


# New game defaults:
var score: int = 0
var lives: int = 3

# These should be saved between runs and loaded on launch:
var high_score: int:
    get:
        return settings_container.settings[Settings.SETTING_HIGH_SCORE]
    set(value):
        settings_container.settings[Settings.SETTING_HIGH_SCORE] = value

var rng_option: Settings.RngOptions:
    get:
        return settings_container.settings[Settings.SETTING_RNG]
    set(value):
        if value in Settings.RngOptions:
            settings_container.settings[Settings.SETTING_RNG] = value
        else:
            print("Invalid RNG option, using default: ", value)
            settings_container.settings[Settings.SETTING_RNG] = Settings.DEFAULT_SETTINGS[Settings.SETTING_RNG]


@onready var settings_container: Settings = Settings.new()
