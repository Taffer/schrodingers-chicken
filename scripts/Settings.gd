class_name Settings
extends Node
## Game settings persistent storage.


enum RngOptions {
    ## 50:50 odds every time.
    RNG_FAIR,

    ## 75% chance of being safe.
    RNG_HUMANE,

    ## 25% chance of being safe.
    RNG_LUCKY,

    ## 75% chance of being safe, goes down when you get a coin, resets to
    ## 75% when you explode.
    RNG_ADAPTIVE,
}


const SETTING_HIGH_DURATION: String = "high_duration"
const SETTING_HIGH_SCORE: String = "high_score"
const SETTING_ONSCREEN_CONTROLS: String = "onescreen_controls"
const SETTING_RNG: String = "rng"
const SETTING_SAW_TUTORIAL: String = "saw_tutorial"

const DEFAULT_SETTINGS: Dictionary = {
    SETTING_HIGH_DURATION: 0,  # Number of seconds survived.
    SETTING_HIGH_SCORE: 0,  # Number of coins found.

    SETTING_ONSCREEN_CONTROLS: false,

    SETTING_RNG: RngOptions.RNG_FAIR,

    SETTING_SAW_TUTORIAL: false,  # Have they seen the tutorial?
}

const SETTINGS_FILE: String = "user://settings.cfg"
const SETTINGS_SECTION: String = "settings"


var settings: Dictionary = {}
var settings_file: ConfigFile = ConfigFile.new()


func _ready() -> void:
    var err: Error = settings_file.load(SETTINGS_FILE)
    if err != OK:
        # Couldn't load it, try to initialize one.
        print("Unable to load settings file, creating a new one.")

        for key in DEFAULT_SETTINGS:
            settings[key] = DEFAULT_SETTINGS[key]
            settings_file.set_value(SETTINGS_SECTION, key, DEFAULT_SETTINGS[key])

            err = settings_file.save(SETTINGS_FILE)
            if err != OK:
                print("Unable to save settings file.")
    else:
        for section: String in settings_file.get_sections():
            if section != SETTINGS_SECTION:
                # Ignore sections we don't know about.
                continue

            for key: String in settings_file.get_section_keys(section):
                settings[key] = settings_file.get_value(section, key)

        # Make sure we've got all the bits that we expect to find. Junk settings
        # will still be there until we save.
        for key in DEFAULT_SETTINGS:
            if key not in settings:
                settings[key] = DEFAULT_SETTINGS[key]


func save_settings() -> Error:
    var new_settings: ConfigFile = ConfigFile.new()
    for key in settings:
        new_settings.set_value(SETTINGS_SECTION, key, settings[key])

    return new_settings.save(SETTINGS_FILE)
