# Schrödinger's Chicken

![Chicken](icon.svg)

A chicken and a quantum superposition.

This is named after
[Schrödinger's Cat](https://en.wikipedia.org/wiki/Schr%C3%B6dinger's_cat), a
famous thought experiment created by an annoyed scientist to show the absurdity
of quantum superposition.

Currently mucking about in [Godot 4](https://godotengine.org/).

## License

*Schrödinger's Chicken* is released under the MIT license; see
[LICENSE.md](LICENSE.md) for details.

The graphics, sounds, fonts, and music are covered by their own specific
licenses; see the individual directories for details.

The chicken head icon is from Kenney's
[Animal Pack Redux](https://kenney.nl/assets/animal-pack-redux), which is
released under a
[CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
license.

[Dialogic](https://dialogic.pro/) is covered by the MIT license; see their
[LICENSE](https://github.com/dialogic-godot/dialogic/blob/main/LICENSE) for
details.
