# Sprite Licenses

`Chicken_Sprite_Sheet.png` by [PixelPlant](https://pixelplant.itch.io/) on
Itch.io:
[https://pixelplant.itch.io/chicken-sprite-sheet](https://pixelplant.itch.io/chicken-sprite-sheet)

![Chicken Sprite Sheet](Chicken_Sprite_Sheet.png)

> Feel free to use these sprites personally or for commercial use.

`ExitIcon.png` and `ExitIconClick.png` by [npkuu](https://npkuu.itch.io/) on
itch.io:
[https://npkuu.itch.io/pixelgui](https://npkuu.itch.io/pixelgui)

`Explosion.png` by [JROB774](https://opengameart.org/users/jrob774) on
OpenGameArt.org:
[https://opengameart.org/content/pixel-explosion-12-frames](https://opengameart.org/content/pixel-explosion-12-frames)

![Explosion](Explosion.png)

`MonedaD.png` by [La Red Games](https://laredgames.itch.io/) on Itch.io, part
of their [Gems / Coins Free pack](https://laredgames.itch.io/gems-coins-free).

![MonedaD](MonedaD.png)

> License Permissions:
>
> - You can use this asset for personal and commercial purpose.
>
> - Credit is not required but would be appreciated.
>
> - Modify as you will.

`treasure chest.png` by [r0ar](https://opengameart.org/users/r0ar) on
OpenGameArt.org:
[https://opengameart.org/content/treasure-chest-sprite](https://opengameart.org/content/treasure-chest-sprite)

![Treasure Chest](treasure chest.png)

> [Public Domain](http://creativecommons.org/publicdomain/zero/1.0/)
