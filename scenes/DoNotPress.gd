extends Area2D


signal you_pushed_it


func push_it():
	$AnimatedSprite2D.play("push")


func _on_animated_sprite_2d_animation_finished():
	if $AnimatedSprite2D.animation == "push":
		you_pushed_it.emit()
