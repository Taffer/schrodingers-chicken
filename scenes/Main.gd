extends Node2D


@onready var score_label: Label = $Score/HBoxContainer/Label
@onready var lives: Array = [
	$Lives/HBoxContainer/Life1,
	$Lives/HBoxContainer/Life2,
	$Lives/HBoxContainer/Life3
]


# Called when the node enters the scene tree for the first time.
func _ready():
	score_label.text = str(Globals.score)


func _on_chicken_area_entered(area):
	if area == $DoNotPress:
		print("CHICKEN CLICKIN'")
		$DoNotPress.push_it()


func _on_chicken_died():
	Globals.lives -= 1
	lives[Globals.lives].visible = false


func _on_you_pushed_it():
	print("OH SHIT YOU PUSHED IT")
	$TreasureChest.open_chest()


func _on_treasure_chest_get_coin():
	Globals.score += 1
	score_label.text = str(Globals.score)


func _on_treasure_chest_opened():
	if randf() < 0.5:
		# Bad path, blow up chicken.
		$Chicken.do_explode()
	else:
		# Good path, get coin!
		$TreasureChest.do_get_coin()
