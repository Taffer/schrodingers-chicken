extends Area2D


signal chest_opened
signal get_coin


func open_chest():
	$AnimatedSprite2D.play("open")


func do_get_coin():
	$Coin.visible = true
	$Timer.start()


func _on_animated_sprite_2d_animation_finished():
	if $AnimatedSprite2D.animation == "open":
		# The Chest has finished opening, signal that the consequences can happen.
		chest_opened.emit()
		$AnimatedSprite2D.frame = 0


func _on_timer_timeout():
	$Coin.visible = false
	get_coin.emit()
