extends Area2D


signal explosion_done


func trigger_explosion():
	self.visible = true
	$AnimatedSprite2D.play("splode")


func _on_animated_sprite_2d_animation_finished():
	if $AnimatedSprite2D.animation == "splode":
		$AnimatedSprite2D.frame = 0
		self.visible = false
		explosion_done.emit()
