# Font Licenses

These fonts are used for the title screen logo:

* [Knewave](https://www.theleagueofmoveabletype.com/knewave) (`knewave.otf`) by
  [Tyler Finck](https://tyfromtheinternet.com/) of the [League of Movable Type](https://www.theleagueofmoveabletype.com/);
  [SIL Open Font license](https://github.com/theleagueof/knewave/blob/master/Open%20Font%20License.markdown).
* [Orbitron](https://www.theleagueofmoveabletype.com/orbitron) (`Orbitron
  Black.otf`) by [Matt McInerney](https://matt.cc/) of the
  [League of Movable Type](https://www.theleagueofmoveabletype.com/);
  [SIL Open Font license](https://github.com/theleagueof/orbitron/blob/master/Open%20Font%20License.markdown).
